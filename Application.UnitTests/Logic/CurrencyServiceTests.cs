using CashMasterPos.Aplication.Common;
using CashMasterPos.Aplication.Common.Interfaces;
using CashMasterPos.Aplication.Exceptions;
using CashMasterPos.Aplication.Services;
using Microsoft.Extensions.Configuration;

namespace Application.UnitTests.Logic
{
    [TestFixture]
    public class CurrencyServiceTests
    {
        private IConfiguration _configuration;
        private readonly ICurrencyService _currencyService;
        private readonly ApplicationSettings _applicationSettings;

        public CurrencyServiceTests()
        {
            _configuration = new ConfigurationBuilder()
             .AddJsonFile("appsettings.json")
             .Build();

            _applicationSettings = ApplicationSettings.GetInstance(_configuration);
            _currencyService = new CurrencyService(_applicationSettings.SystemCurrency);
        }

        [Test]
        [TestCase(500, 500)]
        [TestCase(450, 200)]
        [TestCase(145, 100)]
        [TestCase(45, 20)]
        [TestCase(13, 10)]
        [TestCase(1, 1)]
        [TestCase(1.5, 1)]
        [TestCase(2.5, 2)]
        public void Calculate_MinDenominationGivenDifference_ReturnMinDenomination(decimal difference, decimal expected)
        {
            //Act  
            var minDenomination = _currencyService.GetInmediatelyLowerDenomination(difference);

            //Asert
            Assert.That(minDenomination.Equals(expected));

        }

        [Test]
        [TestCase(0)]
        [TestCase(-2)]
        public void Calculate_MinDenominationGivenDifference_ReturnZeroOrNegativeException(decimal amount)
        {
            //Asert
            Assert.Catch<AmountZeroOrNegativeException>(() => _currencyService.GetInmediatelyLowerDenomination(amount));

        }

        [Test]
        [TestCase("1", 0, true)]
        [TestCase("0.5", 0, true)]
        [TestCase("a", 0, false)]
        [TestCase("-5", 0, false)]
        public void IsValidDenomination_ReturnValidation(string denominationInput, decimal denomination,  bool expected)
        {
            //Act  
            var minDenomination = _currencyService.IsValidDenomination(denominationInput, out denomination);

            //Asert
            Assert.That(minDenomination.Equals(expected));

        }

        [Test]
        [TestCase("3", (ushort)0, true)]
        [TestCase("0.5", (ushort)0, false)]
        [TestCase("1.5", (ushort)0, false)]
        [TestCase("a", (ushort)0, false)]
        [TestCase("-5", (ushort)0, false)]
        public void IsValidQuantityNumber_ReturnValidation(string quantityInput, ushort quantity, bool expected)
        {
            //Act  
            var minDenomination = _currencyService.IsValidQuantityNumber(quantityInput, out quantity);

            //Asert
            Assert.That(minDenomination.Equals(expected));

        }
    }
}