﻿using CashMasterPos.Aplication.Common;
using CashMasterPos.Aplication.Common.Interfaces;
using CashMasterPos.Aplication.Exceptions;
using CashMasterPos.Aplication.Services;
using CashMasterPos.Domain.Entities;
using Microsoft.Extensions.Configuration;

namespace Application.UnitTests.Logic
{
    [TestFixture]
    public class SaleServiceTests
    {
        private readonly ICurrencyService _currencyService;
        private readonly ISaleService _SaleService;
        private IConfiguration _configuration;
        private readonly ApplicationSettings _globalConfiguration;

        public SaleServiceTests()
        {
            _configuration = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json")
            .Build();

            _globalConfiguration = ApplicationSettings.GetInstance(_configuration);
            _currencyService = new CurrencyService(_globalConfiguration.SystemCurrency);
            _SaleService = new SaleService();
        }

        [Test]
        [TestCase("1", 0, true)]
        [TestCase("10.5", 0, true)]
        [TestCase("0", 0, false)]
        [TestCase("a", 0, false)]
        [TestCase("-5", 0, false)]
        public void IsValidProductPrice_ReturnValidation(string productPriceInput, decimal productPrice, bool expected)
        {
            //Act  
            var minDenomination = _SaleService.IsValidProductPrice(productPriceInput, out productPrice);

            //Asert
            Assert.That(minDenomination.Equals(expected));

        }

        [Test]
        public void CalculateReturnChange_Bill355_Pay500_Return145()
        {
            //Arrange
            var sale = new Sale
            {
                TotalBill = 355,
                TotalPayment = 500
            };

            //Act
            var changeDue = _SaleService.CalculateReturnChange(sale, _currencyService);

            //Assert
            Assert.That(changeDue.Count, Is.EqualTo(3));
            Assert.That(changeDue[100.0M], Is.EqualTo(1));
            Assert.That(changeDue[20.0M], Is.EqualTo(2));
            Assert.That(changeDue[5.0M], Is.EqualTo(1));
        }

        [Test]
        public void CalculateReturnChange_Bill105p5_Pay200_Return94p5()
        {
            //Arrange
            var sale = new Sale
            {
                TotalBill = 105.5M,
                TotalPayment = 200
            };

            //Act
            var changeDue = _SaleService.CalculateReturnChange(sale, _currencyService);

            //Assert
            Assert.That(changeDue.Count, Is.EqualTo(4));
            Assert.That(changeDue[50.0M], Is.EqualTo(1));
            Assert.That(changeDue[20.0M], Is.EqualTo(2));
            Assert.That(changeDue[0.5M], Is.EqualTo(1));
        }

        [Test]
        public void CalculateReturnChange_Bill400_Pay400_ReturnEmptyDiccionary()
        {
            //Arrange
            var sale = new Sale
            {
                TotalBill = 400,
                TotalPayment = 400
            };

            //Act
            var changeDue = _SaleService.CalculateReturnChange(sale, _currencyService);

            //Assert
            Assert.That(changeDue.Count, Is.EqualTo(0));
        }


        [Test]
        public void CalculateReturnChange_Bill500_Pay300_ThrowSaleNegativeDifferenceException()
        {
            //Arrange
            var sale = new Sale
            {
                TotalBill = 500,
                TotalPayment = 300
            };

            //Assert
            Assert.Catch<SaleNegativeDifferenceException>(() => _SaleService.CalculateReturnChange(sale, _currencyService));
        }

        [Test]
        public void CalculateReturnChange_PassNullSale_ThrowNullArgumentException()
        {
            //Arrange
            Sale sale = null;

            //Assert
            Assert.Catch<ArgumentNullException>(() => _SaleService.CalculateReturnChange(sale, _currencyService));
        }

        [Test]
        public void CalculateReturnChange_PassNullCurrencyService_ThrowNullArgumentException()
        {
            //Arrange
            var sale = new Sale
            {
                TotalBill = 500,
                TotalPayment = 300
            };
            CurrencyService currencyService = null;

            //Assert
            Assert.Catch<ArgumentNullException>(() => _SaleService.CalculateReturnChange(sale, null));
        }
    }
}
