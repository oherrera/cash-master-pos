# CASH MASTER POS SOLUTION
Author: Ing. Octavio Herrera Hern�ndez
Date: 2023-01-27


## Folder estructure

-CashMasterPos
|
-- -**Documentation**: this Readme.md file.
-- -**Src**: Application code.
-- -**Tests**: Unit Tests


## Solution description
Implement a point-of-sale module that calculates the correct/optimal change to be given to customers.

### Justification
Today�s young cashiers are increasingly unable to return the correct amount of change.

### General acceptance criteria

> Calculate the correct change and display the optimum (i.e. minimum) number of bills and coins to return to the customer.

## Functional Requeriments

 - Capture prices of the item(s) being purchased.
 - Capture Bills and coins (denominations) given by a client to pay for the item(s).
 - Calculate the appropiate change to be returned with minimal denominations number of coins or bills.
 - Currency could be changed through a global configuration file thats include the denomination to use by the system.

### Limitations

 - It is not required to phisically distinguish whether the values are bills or coins, only their numeric.

## Non-functional requeriments.

 - It must be a C# console app.
 - It must have comments to help future engineers work with application.
 - A unit tests must be included to ensure functionality of change calculation engine.
 - Best practices and use of principles of OOP.
 - Performance optimization.
 - Clairly exception handlers.

## Documentation

 - You need to configure the **appsettings.json** -> section "SystemCurrency" according to the country where system will be implemented.
 - See the document "Cash Master POS Solution.pdf" included in GIT repo to check high level technical details.