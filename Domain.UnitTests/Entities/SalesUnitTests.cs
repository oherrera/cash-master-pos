using CashMasterPos.Domain.Entities;

namespace Domain.UnitTests.Entities
{
    [TestFixture]
    public class SalesUnitTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void CheckIsPaymentComplete_PaymentMinorThanBill_ReturnFalse()
        {
            //Arrange
            var sale = new Sale
            {
                TotalBill = 500,
                TotalPayment = 200
            };

            //Act
            bool isPaymentComplete = sale.IsPaymentComplete;

            //Assert
            Assert.IsFalse(isPaymentComplete);
        }

        [Test]
        public void CheckIsPaymentComplete_PaymentMayorThanBill_ReturnTrue()
        {
            //Arrange
            var sale = new Sale
            {
                TotalBill = 500,
                TotalPayment = 600
            };

            //Act
            bool isPaymentComplete = sale.IsPaymentComplete;

            //Assert
            Assert.IsTrue(isPaymentComplete);
        }


        [Test]
        public void CheckIsPaymentComplete_PaymentEqualThanBill_ReturnTrue()
        {
            //Arrange
            var sale = new Sale
            {
                TotalBill = 500,
                TotalPayment = 500
            };

            //Act
            bool isPaymentComplete = sale.IsPaymentComplete;

            //Assert
            Assert.IsTrue(isPaymentComplete);
        }

        [Test]
        public void CheckIsPaymentComplete_PaymentTotalBillZero_ReturnTrue()
        {
            //Arrange
            var sale = new Sale
            {
                TotalBill = 0,
                TotalPayment = 0
            };

            //Act
            bool isPaymentComplete = sale.IsPaymentComplete;

            //Assert
            Assert.IsTrue(isPaymentComplete);
        }
    }
}