﻿using CashMasterPos.Domain.Entities;
using Microsoft.Extensions.Configuration;


namespace CashMasterPos.Aplication.Common
{
    public class ApplicationSettings
    {
        private static ApplicationSettings instance;
        private static object instanceLock = new object();
        private static IConfiguration _configuration;

        public static ApplicationSettings GetInstance(IConfiguration configuration)
        {
            if (instance == null)
            {
                lock (instanceLock)
                {
                    if (instance == null)
                        instance = new ApplicationSettings(configuration);
                }
            }

            return instance;
        }

        public Currency SystemCurrency { get; private set; }

        private ApplicationSettings(IConfiguration config)
        {
            _configuration = config;
            SetCurrency();
        }

        private void SetCurrency()
        {
            try
            {
                SystemCurrency = _configuration.GetSection("SystemCurrency").Get<Currency>();
                if (SystemCurrency == null)
                {
                    //In case configuration not exists, use default configuration USD
                    SystemCurrency = new Currency
                    {
                        Code = "USD",
                        Denominations = new List<decimal> { 0.01M, 0.05M, 0.10M, 0.25M, 0.50M, 1, 2, 5, 10, 20, 50, 100 }
                    };
                }

                //Order by Desc to use later in Change algorithm 
                SystemCurrency.Denominations = SystemCurrency.Denominations
                    .OrderByDescending(c => c).ToList();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
