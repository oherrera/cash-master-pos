﻿namespace CashMasterPos.Aplication.Common.Interfaces
{
    public interface ICurrencyService
    {
        
        public List<decimal> SystemCurrencyDenominations { get; }

        public bool IsValidDenomination(string denomination, out decimal denominationReceived);
        public bool IsValidQuantityNumber(string quantity, out ushort quantityNumber);
        public decimal GetInmediatelyLowerDenomination(decimal amount);
    }
}
