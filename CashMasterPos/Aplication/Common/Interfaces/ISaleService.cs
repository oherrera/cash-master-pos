﻿using CashMasterPos.Domain.Base;
using CashMasterPos.Domain.Entities;

namespace CashMasterPos.Aplication.Common.Interfaces
{
    public interface ISaleService
    {
        public bool IsValidProductPrice(string productPriceInput, out decimal productPriceDecimal);
        public DenominationQuantity<decimal, ushort> CalculateReturnChange(Sale sale, ICurrencyService currencyService);
      
    }
}
