﻿using CashMasterPos.Aplication.Common.Interfaces;
using CashMasterPos.Aplication.Exceptions;
using CashMasterPos.Domain.Base;
using CashMasterPos.Domain.Entities;
using System.Drawing;

namespace CashMasterPos.Aplication.Services
{
    public class SaleService : ISaleService
    {
        public SaleService()
        {

        }

        //A product price couldn't be zero or lower
        public bool IsValidProductPrice(string productPriceInput, out decimal productPriceDecimal)
        {
            var isValidDecimalNumber = decimal.TryParse(productPriceInput, out productPriceDecimal);

            if (!isValidDecimalNumber)
                return false;

            if (productPriceDecimal <= 0)
                return false;

            return true;
        }

        /// <summary>
        /// MAIN Rutine to Calculate the ideal change to give according to the payment received and system currency.
        /// </summary>
        /// <param name="sale">The actual sale that function requires to calculate change</param>
        /// <param name="currencyService">The currency service which application is using</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">If sale or currency service is null</exception>
        /// <exception cref="SaleNegativeDifferenceException">If the difference between total payment and total bill is zero or minus</exception>
        public DenominationQuantity<decimal, ushort> CalculateReturnChange(Sale sale, ICurrencyService currencyService)
        {
            try
            {
                if(sale == null)
                    throw new ArgumentNullException("Sale", $"Sale not found, system can not do the change calculate");

                if (currencyService == null)
                    throw new ArgumentNullException("Currency serivce", $"Currency service not found, can not do the calculate.");

                var difference = sale.TotalPayment - sale.TotalBill;

                if (difference < 0)
                    throw new SaleNegativeDifferenceException();

                //Denomination, quantity of bills/coins
                var changeToReturn = new DenominationQuantity<decimal, ushort>();

                while (difference != 0)
                {
                    var denomination = currencyService.GetInmediatelyLowerDenomination(difference);

                  
                    if (denomination == 0.0M) 
                        break; //No denominations exists. At this point difference is greater than 0 but less than the minimum denomination

                    if (!changeToReturn.ContainsKey(denomination))
                        changeToReturn.Add(denomination, 1);
                    else
                        changeToReturn[denomination]++;

                    difference -= denomination;
                }

                return changeToReturn;
            }
            catch (ArgumentNullException)
            {
                throw;
            }
            catch (AmountZeroOrNegativeException)
            {
                throw;
            }
            catch (Exception) 
            {
                throw;
            }
        }

       
    }
}
