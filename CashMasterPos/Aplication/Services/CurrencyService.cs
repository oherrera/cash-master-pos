﻿using CashMasterPos.Aplication.Common.Interfaces;
using CashMasterPos.Aplication.Exceptions;
using CashMasterPos.Domain.Entities;

namespace CashMasterPos.Aplication.Services
{
    public class CurrencyService : ICurrencyService
    {
        private readonly Currency _currency;
        public CurrencyService(Currency currency)
        {
            if(currency == null)
                throw new ArgumentNullException("Currency", "System must obtain a currency configuration.");

            if (currency.Denominations == null)
                throw new NoDenominationFoundException();


            _currency = currency;
        }

        public List<decimal> SystemCurrencyDenominations => _currency.Denominations ?? 
            throw new NoDenominationFoundException();

        /// <summary>
        /// Gets the immediate lower denomination given an amount.
        /// </summary>
        /// <param name="amount">Amount given</param>
        /// <returns>The encountered lower denomination</returns>
        /// <exception cref="AmountZeroOrNegativeException">If the given amount is lower or equal than zero</exception>
        public decimal GetInmediatelyLowerDenomination(decimal amount)
        {
            try
            {
                if (amount <= 0)
                    throw new AmountZeroOrNegativeException();

                decimal lowerDenomination = 0.0M;
                foreach(decimal denomination in _currency.Denominations)
                {
                    if (denomination <= amount)
                    {
                        lowerDenomination = denomination;
                        break;
                    }
                }
               
                return lowerDenomination;
            }
            catch(ArgumentNullException)
            {
                throw;
            }
            catch(Exception) 
            {
                throw;
            }
        }


        public bool IsValidDenomination(string denomination, out decimal denominationDecimal)
        {
            var isValidDecimalNumber = decimal.TryParse(denomination, out denominationDecimal);

            if (!isValidDecimalNumber)
                return false;

            if (!_currency.Denominations.Contains(denominationDecimal))
                return false;

            return true;

        }
        public bool IsValidQuantityNumber(string quantity, out ushort quantityNumber)
        {
            var isValidQuantityNumber = ushort.TryParse(quantity, out quantityNumber);

            if (!isValidQuantityNumber)
                return false;

            return true;

        }
    }
}
