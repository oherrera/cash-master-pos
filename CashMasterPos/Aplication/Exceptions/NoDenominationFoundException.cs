﻿
namespace CashMasterPos.Aplication.Exceptions
{
    internal class NoDenominationFoundException : Exception
    {
        public NoDenominationFoundException() : base("No currency or denomination found in the system configuration, please contact support")
        {

        }
    }
}
