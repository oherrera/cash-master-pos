﻿
namespace CashMasterPos.Aplication.Exceptions
{
    internal class WorkFlowException : Exception
    {
        public WorkFlowException() : base("An error ocurred during the workflow. Contact support.")
        {

        }
    }
}
