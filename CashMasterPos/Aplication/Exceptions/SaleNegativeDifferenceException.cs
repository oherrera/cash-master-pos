﻿
namespace CashMasterPos.Aplication.Exceptions
{
    public class SaleNegativeDifferenceException : Exception
    {
        public SaleNegativeDifferenceException() : base("The difference between total payment and total bill is a negative number, it is not possible to calculate")
        {

        }
    }
}
