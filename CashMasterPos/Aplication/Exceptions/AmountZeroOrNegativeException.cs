﻿
namespace CashMasterPos.Aplication.Exceptions
{
    public class AmountZeroOrNegativeException : Exception
    {
        public AmountZeroOrNegativeException() : base("The amount is zero or negative, it couldn't use for calculate")
        {

        }
    }
}
