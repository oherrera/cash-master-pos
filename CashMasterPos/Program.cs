﻿using CashMasterPos.Aplication.Common;
using CashMasterPos.Aplication.Exceptions;
using CashMasterPos.Aplication.Services;
using CashMasterPos.Presentation;
using Microsoft.Extensions.Configuration;

namespace CashMasterPos
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
              .SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile("appsettings.json", optional: false);

            try
            {

                IConfiguration config = builder.Build();
                
                //Get global configuration once
                var applicationSettings = ApplicationSettings.GetInstance(config);

                //Create Services instances to be injected in application.
                var saleServices = new SaleService();
                var currencyService = new CurrencyService(applicationSettings.SystemCurrency);

                
                var app = new Application(saleServices, currencyService);
                app.Start();

            }
            catch (ArgumentNullException ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);

                Console.ReadKey();
                Environment.Exit(0);
            }
            catch (NoDenominationFoundException ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);

                Console.ReadKey();
                Environment.Exit(0);
            }
            catch (WorkFlowException ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);

                Console.ReadKey();
                Environment.Exit(0);
            }
            catch(Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"An error ocurrer trying obtaining the file configuration, " +
                    $"check the file configuration or ask for support. Application can not start.");

                Console.ReadKey();
                Environment.Exit(0);
            }

            

        }
    }
}