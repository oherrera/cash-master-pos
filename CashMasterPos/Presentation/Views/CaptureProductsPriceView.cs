﻿using CashMasterPos.Aplication.Common.Interfaces;
using CashMasterPos.Domain.Entities;
using CashMasterPos.Presentation.Helpers;

namespace CashMasterPos.Presentation.Views
{
    public class CaptureProductsPriceView : BaseView
    {
        private readonly Sale _sale;
        private readonly ISaleService _salesService;

        public CaptureProductsPriceView(
            Sale sale,
            ISaleService salesService)
        {
            _sale = sale;
            _salesService = salesService;
        }

        public override void Start()
        {
            ShowWelcome();

            _sale.TotalBill = 0.0M;
            ushort itemsCount = 1;

            ConsoleMessage.WriteInfoMessage("Capture de prices of the producs purchased");
          
            ConsoleMessage.WriteInfoMessage("Type ", TypeLine.Same);
            ConsoleMessage.WriteSuccessMessage("F ", TypeLine.Same);
            ConsoleMessage.WriteInfoMessage("to finish capture. ", TypeLine.New);
            ConsoleMessage.WriteInfoMessage("", TypeLine.New);

            while (true)
            {
                ConsoleMessage.WriteInfoMessage($"Type the price of the item {itemsCount} purchased: ", TypeLine.Same);

                Console.ForegroundColor = ConsoleColor.Green;
                decimal productPrice;
                var productPriceInput = Console.ReadLine();

                if (productPriceInput == "F" || productPriceInput == "f")
                    break;

                var isValidProductPrice = _salesService.IsValidProductPrice(productPriceInput, out productPrice);
                if (isValidProductPrice)
                {
                    _sale.TotalBill += productPrice;
                    itemsCount++;
                }
                else
                    ConsoleMessage.WriteErrorMessage("The specified amount is not correct, it must be a positive number or F to finish capture, try again");
            }
        }
    }
}
