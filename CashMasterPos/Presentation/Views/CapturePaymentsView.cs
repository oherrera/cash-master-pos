﻿using CashMasterPos.Aplication.Common.Interfaces;
using CashMasterPos.Domain.Entities;
using CashMasterPos.Presentation.Helpers;

namespace CashMasterPos.Presentation.Views
{
    public class CapturePaymentsView : BaseView
    {
        private readonly Sale _sale;
        private ICurrencyService _currencyService;


        public CapturePaymentsView(
            Sale sale,
            ICurrencyService currencyService
            )
        {
            _sale = sale;
            _currencyService = currencyService;
        }

        public override void Start()
        {
            ShowWelcome();

            ConsoleMessage.WriteInfoMessage("Total Bill: ", TypeLine.Same);
            ConsoleMessage.WriteSuccessMessage($"{_sale.TotalBill:c}");

            ConsoleMessage.WriteInfoMessage("Capture bills and coins received according to next list of denominations: ");
            ConsoleMessage.WriteInfoMessage(string.Join($" | ", _currencyService.SystemCurrencyDenominations));

            ConsoleMessage.WriteInfoMessage("========================================================================");

            while (!_sale.IsPaymentComplete)
            {
              
                ConsoleMessage.WriteInfoMessage("Denomination received: ", TypeLine.Same);
                Console.ForegroundColor = ConsoleColor.Green;

                //Capture denomination received
                decimal denominationReceived;
                var denominationReceivedInput = Console.ReadLine();
                var isValidDenominationInput = _currencyService.IsValidDenomination(denominationReceivedInput, out denominationReceived);
                
                if (isValidDenominationInput)
                {
                    bool isValidQuantityInput = false;
                    while (!isValidQuantityInput)
                    {
                        ConsoleMessage.WriteInfoMessage($"Number of {denominationReceived:c} bills/coins received: ", TypeLine.Same);
                        Console.ForegroundColor = ConsoleColor.Green;

                        //Capture denomination quantity received
                        ushort quantityReceived;
                        var quantityInput = Console.ReadLine();
                        isValidQuantityInput = _currencyService.IsValidQuantityNumber(quantityInput, out quantityReceived);

                        if (isValidQuantityInput)
                        {
                            //Denomination and quantity must be valid.
                            _sale.TotalPayment += denominationReceived * quantityReceived;

                            AddPaymentRelation(denominationReceived, quantityReceived);

                            ConsoleMessage.WriteInfoMessage("-Total Bill: ", TypeLine.Same);
                            ConsoleMessage.WriteSuccessMessage($"{_sale.TotalBill:c} ", TypeLine.Same);

                            if (_sale.IsPaymentComplete)
                                ShowPaymentCompleteMessage();
                            else
                                ShowPaymentIncompleteMessage();
                        }
                        else
                            ConsoleMessage.WriteErrorMessage("The input is incorrect, you must capture a positive number ");
                    }

                }
                else
                    ConsoleMessage.WriteErrorMessage("Invalid input, you must capture a valid denomination");
            }
        }

        public void AddPaymentRelation(decimal denomination, ushort quantity)
        {
            if (_sale.PaymentRelation.ContainsKey(denomination))
                _sale.PaymentRelation[denomination] += quantity;
            else
                _sale.PaymentRelation.Add(denomination, quantity);
        }

        private void ShowPaymentIncompleteMessage()
        {
            ConsoleMessage.WriteInfoMessage($"-Total Received: ", TypeLine.Same);
            ConsoleMessage.WriteErrorMessage($"{_sale.TotalPayment:c} ", TypeLine.Same);

            ConsoleMessage.WriteInfoMessage($"-Rest: ", TypeLine.Same);
            ConsoleMessage.WriteWarningMessage($"{_sale.RestToPay:c} ", TypeLine.New);
            ConsoleMessage.WriteInfoMessage("", TypeLine.New);
        }

        private void ShowPaymentCompleteMessage()
        {
            ConsoleMessage.WriteInfoMessage("");
            ConsoleMessage.WriteInfoMessage("");
            ConsoleMessage.WriteSuccessMessage("PAYMENT COMPLETE");
            ConsoleMessage.WriteInfoMessage($"-Total Received: ", TypeLine.Same);
            ConsoleMessage.WriteSuccessMessage($"{_sale.TotalPayment:c} ", TypeLine.Same);

            ConsoleMessage.WriteInfoMessage($"-Total change: ", TypeLine.Same);
            ConsoleMessage.WriteWarningMessage($"{_sale.TotalChange:c} ", TypeLine.New);

            ConsoleMessage.WriteInfoMessage("");
            ConsoleMessage.WriteWarningMessage("The payment is complete!, press ANY KEY to continue and calculate bills and coins.");

            Console.ReadKey();
        }
      
    }
}
