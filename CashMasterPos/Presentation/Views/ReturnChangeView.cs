﻿using CashMasterPos.Aplication.Common.Interfaces;
using CashMasterPos.Domain.Entities;
using CashMasterPos.Presentation.Helpers;

namespace CashMasterPos.Presentation.Views
{
    public class ReturnChangeView : BaseView
    {
        private readonly Sale _sale;
        private readonly ISaleService _salesService;
        private readonly ICurrencyService _currencyService;
        public ReturnChangeView(
            Sale sale, 
            ISaleService salesService, 
            ICurrencyService currencyService) 
        { 
            _sale = sale;
            _salesService = salesService;
            _currencyService = currencyService;
        }

        public override void Start()
        {
            try
            {
                //Change to return calculation
                _sale.ChangeRelation = _salesService.CalculateReturnChange(_sale, _currencyService);

                //Resume of the bills and coins received.
                ConsoleMessage.WriteInfoMessage("================================== ");
                ConsoleMessage.WriteInfoMessage("You received: ");
                ConsoleMessage.WriteInfoMessage("");
                foreach (var denomination in _sale.PaymentRelation)
                    ConsoleMessage.WriteInfoMessage($" - {denomination.Value} of {denomination.Key:c}");
                ConsoleMessage.WriteInfoMessage("");


                if (_sale.ChangeRelation.Count == 0)
                    ConsoleMessage.WriteSuccessMessage("No change to give, say thanks for the purchase");
                else
                {
                    //Resume of the bills and coins to return.
                    ConsoleMessage.WriteInfoMessage("================================== ");
                    ConsoleMessage.WriteInfoMessage("This is the ideal change to give: ");
                    ConsoleMessage.WriteInfoMessage("");

                    foreach (var denomination in _sale.ChangeRelation)
                        ConsoleMessage.WriteInfoMessage($" - {denomination.Value} of {denomination.Key:c}");

                    ConsoleMessage.WriteInfoMessage("");
                    ConsoleMessage.WriteWarningMessage("Say thanks for the puchase.");
                }
            }
            catch (ArgumentNullException ex)
            {
                ConsoleMessage.WriteErrorMessage($"Error: an error ocurred during the operation: {ex.Message}");
            }
            catch(Exception)
            {
                throw;
            }
        }
    }
}

