﻿using CashMasterPos.Presentation.Helpers;


namespace CashMasterPos.Presentation.Views
{
    public abstract class BaseView
    {
        public void ShowWelcome()
        {
            ConsoleMessage.WriteInfoMessage("WELCOME TO CASH MASTERS POS");
            ConsoleMessage.WriteInfoMessage("==================================");
        }

        public abstract void Start();
    }
}
