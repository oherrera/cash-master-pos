﻿
namespace CashMasterPos.Presentation.Helpers
{
    public static class ConsoleMessage
    {
        public static void WriteInfoMessage(string message, TypeLine enumTypeLine = TypeLine.New)
        {
            Console.ForegroundColor = ConsoleColor.Gray;

            if(enumTypeLine == TypeLine.Same)
                Console.Write(message);
            if(enumTypeLine == TypeLine.New)
                Console.WriteLine(message);
        }

        public static void WriteSuccessMessage(string message, TypeLine enumTypeLine = TypeLine.New)
        {
            Console.ForegroundColor = ConsoleColor.Green;

            if (enumTypeLine == TypeLine.Same)
                Console.Write(message);
            if (enumTypeLine == TypeLine.New)
                Console.WriteLine(message);
        }

        public static void WriteWarningMessage(string message, TypeLine enumTypeLine = TypeLine.New)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;

            if (enumTypeLine == TypeLine.Same)
                Console.Write(message);
            if (enumTypeLine == TypeLine.New)
                Console.WriteLine(message);
        }

        public static void WriteErrorMessage(string message, TypeLine enumTypeLine = TypeLine.New)
        {
            Console.ForegroundColor = ConsoleColor.Red;

            if (enumTypeLine == TypeLine.Same)
                Console.Write(message);
            if (enumTypeLine == TypeLine.New)
                Console.WriteLine(message);
        }

    }
}
