﻿
using CashMasterPos.Aplication.Common.Interfaces;
using CashMasterPos.Aplication.Exceptions;
using CashMasterPos.Domain.Entities;
using CashMasterPos.Presentation.Helpers;
using CashMasterPos.Presentation.Views;

namespace CashMasterPos.Presentation
{
    public class Application
    {
        private readonly ISaleService _salesService;
        private readonly ICurrencyService _currencyService;

        public Application(
            ISaleService saleService, 
            ICurrencyService currencyService)
        {
            if (saleService == null)
                throw new ArgumentNullException("Sale Service", "There is no sale service available in application. Can not continue.");
            if (currencyService == null)
                throw new ArgumentNullException("Currency service", "There is no currency service available in application. Can not continue.");


            _salesService = saleService;
            _currencyService = currencyService;
        }

        public void Start()
        {
            while(true)
            {
                //START WORKFLOW
                Console.Clear();
                var sale = new Sale();

                //STEP 1: Capture product(s) prices:
                StartCaptureProductsPriceView(sale, _salesService);

                if(sale.TotalBill == 0)
                    ConsoleMessage.WriteWarningMessage("Nothing to pay", TypeLine.New);
                else
                {
                    //STEP 2: Capture payment
                    StartCapturePaymentsView(sale, _currencyService);

                    //STEP 3: Calculate change
                    StartReturnChageView(sale, _salesService, _currencyService);
                }

                Console.ReadKey();
                ConsoleMessage.WriteInfoMessage("");
                ConsoleMessage.WriteInfoMessage("Do you want to do another sale? (Y: Yes / Any Key: Exit application): ", TypeLine.Same);
                Console.ForegroundColor = ConsoleColor.Green;

                var readUser = Console.ReadKey();
                if (readUser.Key != ConsoleKey.Y)
                    break;
            }

            Environment.Exit(0);
        }

        private void StartCaptureProductsPriceView(Sale sale, ISaleService saleService)
        {
            try
            {
                BaseView captureProductsView = new CaptureProductsPriceView(sale, saleService);
                captureProductsView.Start();
                Console.Clear();
            }
            catch (Exception)
            {
                throw new WorkFlowException();
            }
        }

        private void StartCapturePaymentsView(Sale sale, ICurrencyService currencyService)
        {  
            try
            {
                BaseView capturePaymentsView = new CapturePaymentsView(sale, currencyService);
                capturePaymentsView.Start();
            }
            catch(NoDenominationFoundException ex)
            {
                ConsoleMessage.WriteErrorMessage(ex.Message);
            }
            catch (Exception)
            {
                throw new WorkFlowException();
            }

        }

        private void StartReturnChageView(Sale sale, ISaleService saleService, ICurrencyService currencyService)
        {
            try
            {
                BaseView returnChangeView = new ReturnChangeView(sale, saleService, currencyService);
                returnChangeView.Start();
            }
            catch (Exception)
            {
                throw new WorkFlowException();
            }
        }
    }
}
