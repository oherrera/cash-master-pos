﻿
namespace CashMasterPos.Domain.Base
{
    /// <summary>
    /// Custom dictionary for entity Sale
    /// </summary>
    /// <typeparam name="TKey">Denomination</typeparam>
    /// <typeparam name="TValue">Quantity</typeparam>
    public class DenominationQuantity<TKey, TValue> : Dictionary<TKey, TValue>
    {
    }
}
