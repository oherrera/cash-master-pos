﻿
namespace CashMasterPos.Domain.Entities
{
    public class Currency
    {
        public string Code { get; set; }
        public List<decimal> Denominations { get; set; }
    }
}
