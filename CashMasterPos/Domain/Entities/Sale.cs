﻿
using CashMasterPos.Domain.Base;

namespace CashMasterPos.Domain.Entities
{
    public class Sale
    {
        public Sale() 
        {
            PaymentRelation = new DenominationQuantity<decimal, ushort>();
            ChangeRelation= new DenominationQuantity<decimal, ushort>(); 
        }   
        public decimal TotalBill { get; set; }
        public decimal TotalPayment { get; set; }

        public decimal RestToPay => TotalBill - TotalPayment;
        public decimal TotalChange => TotalPayment - TotalBill;
        public bool IsPaymentComplete => (TotalBill > 0) ? TotalPayment >= TotalBill : true;


        public DenominationQuantity<decimal, ushort> PaymentRelation { get; set; }
        public DenominationQuantity<decimal, ushort> ChangeRelation { get; set; }




  
    }
}
